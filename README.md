# Robot To Frame

## Dependencies

You will need the following custom packages : 

* ros_wrap : a package specialized in loading ros parameters (https://gitlab.inria.fr/auctus-team/people/erwannlandais/public/ros_management_ws/ros_wrap)
* tf_management_pkg : a package specialized in managing SE3 ROS components (Pose, Transform, ...) (https://gitlab.inria.fr/auctus-team/people/erwannlandais/public/ros_management_ws/tf_management_pkg)

## How to use

### General explanations

We consider that you have 2 TF trees : 

robot_parent_id ==> robot_id : the TF tree of the robot.

TF_parent_id ==> TF_id : the TF tree of the frame.

We want to find the following connexion : 

robot_parent_id ==> TF_id

The procedure is the following : 

* Select a set of points. These points must be reachable by the robot's end-effector, and must be expressed in a reference frame that you can measure (TF_id). 
Those are examples of pointclouds : 
    - A set of Optitrack markers corresponding to a rigid body.
    - Remarkables points on a rigid body (ex : table)
* Put the end-effector of the robot on each of those points. Save the position of the EE for each of those points.
* Express those points into TF_id.
* Find the correspondences between the points expressed with the EE and with TF_id.
* Get the transform between the pointclouds.
* Use this transform to get the robot_parent_id ==> TF_id transform.

### Set up your .yaml

You need to set different parameters for each frame for which you want a calibration. Those parameters need to be put in a .yaml file, and are the following :  

* robot_parent_id: the parent frame which you want to connect to TF_id. This frame should be connected to robot_id.
* robot_id: the child frame corresponding to the frame used to capture the position of the different markers. 
* TF_parent_id: the parent frame of TF_id. 
* TF_id: the frame in which markers are expressed. 
* markers: positions of markers expressed in TF_id. You need at least 3 markers.
* useICPForCorres : boolean, which triggers ICP to find the correspondences between points.
    - If set to False, we consider that the order of points to be reached by the EE is the same than the order of points in markers.
    - Do not set to True if your markers are too close to each of them (< 10 cm)
* putTransformInTF: boolean. Controls if the robot_parent_id ==> TF_id should be put into the TF tree after the calibration.

Those .yaml files need to be put in config folder. You need to load them as parameter of r2TF_calib executable (from robot_to_frame.cpp file).

### To launch calibration

First, launch a spawn of the .urdf of the real Panda. (e.g a robot_description parameter of the Panda with its calibration_tip, that can be displayed on Rviz, working when Panda is in gravity compensation). 

~~~
roslaunch panda_qp_simple_control velocity_control_tracking.launch sim:=false load_gripper:=false load_calibration_tip:=true
~~~

(with FCI activated, and stop activated. This is just to launch the simulation of the articular configuration of Panda in real time).

Then, activates the calibration to a frame with a launchfile : 
~~~
roslaunch robot_to_frame [launchfile]
~~~

Different templates of launchfiles are available in launch folder.

To start calibration, call the following service :
~~~

rosservice call /R2TF_calib_trigger

~~~

Follow the instructions in terminal, and then, save transform with : 

~~~
rosservice call /R2TF_save_tf "<robot_parent_id>#<TF_id>"
~~~

Which will be saved in $(find robot_to_frame)/tf_transf/<robot_parent_id>_to_<TF_id>.yaml

You can now use this transform with complete_hardware.launch.

### To save a TF

rosservice call /R2TF_save_tf "[parent_TF]#[child_TF]"

TF are saved as .yaml into folder given by "output_tf_path" parameter. The format of this .yaml is [parent_TF]_-_to_-_[child_TF].yaml.

## Basic procedure

### Create a rigid body in Motive which origin is matching the position of a specific marker

This can be pretty useful to check the transform between the rigid body and the robot by putting the calibration tip on the marker representing this origin.

To do so, in Motive : 

- View > Builder Pane > Edit
- Click on the concerned rigid body in Perspective View
- Do the Refine procedure. You will need to deplace your rigid body in the volume while doing it, but you will get more stable and accurate measurements of its transform.
- View > Properties Pane
- Change X-Y-Z locations in Builder Pane, such as the position of one of the markers in the Properties Pane is as close as possible of 0. You should normally have at best a precision inferior to 0.5mm on each axis.

### Calib from robot base to table 

#### Where to put remarkable points for calibration

You can :

* Set them by theory, with table dimensions

* Put the tip at different remarkable points of the table (corners, or a distance from a corner), and note the distance between the different points.

Here, points are saved into table_test.yaml as : 

* Point at bottom right corner (with a view from top)
* Point at top right corner
* Point at bottom right corner with a 10 cm translation on Y.

#### How to get transform from robot to table

You need the table_true => panda_link0 transform. You need to apply it to velocity_control.launch once you get it.

### Calib from table to square_calib_x

You need the table => square_calib_x transform. 

You can also be able to get the world => world_optitrack transform that you need.

## Application to Panda

Let's say you just start with your robot and your calibration rigid body (ex : square_calib_x). Optitrack cameras are already calibrated. The calibration tip is put on the robot, and everything is set up.

You also need : 
* To have a good approximation of the dimensions of the table (with a .dae model). Use Optitrack markers to get those measurements.
* To have a good approximation of the dimensions of the calibration tip (normally ok).

### Get the transform between the robot and the table

#### How to check 

Place the EE on the corner of the table. Check if its position on Rviz is sufficiently matching with reality.

#### How to do

- Launch the .urdf simulation :

~~~
roslaunch panda_qp_simple_control control_tracking.launch sim:=false load_gripper:=false load_calibration_tip:=true
~~~

- Once everything is launched, press emergency stop button. You must still have an update of the Panda /tf. 
- Start calibration by doing : 

~~~
roslaunch robot_to_frame calibrate_table.launch

rosservice call /R2TF_calib_trigger
~~~

- Do the calibration process
- Get the transform from table_true to panda_link0 :
    * Approximately with :
        ~~~
            rosrun tf tf_echo table_true panda_link0
        ~~~
    * With maximum precision with : 
        ~~~
            rosservice call /R2TF_save_tf "table_true#panda_link0"
        ~~~
        - Transform will be saved in $(find robot_to_frame)/tf_transf/table_true_to_panda_link0.yaml

- Note the translation value in control_tracking.launch, at "x", "y", and "z" arguments.
- Check if those values are good by restarting the steps from "Launch the .urdf simulation with..." to "roslaunch robot_to_frame calibrate_table.launch". 

### Get the transform between the table and the Optitrack calibration tool (placed at a corner)

#### How to check

Control that the Motive /tf convention (Y-up or Z-up) corresponds to what you're looking for. This calibration step will be correct only for its currently selected type of convention. 

If the calibration process is not made yet (e.g you don't have the world <-> world_optitrack relationship), do the classic calibration with optitrack calibration tool (aka square_calib_x rigid body), on the right of the table : 

~~~
roslaunch optitrack_to_robot calibrate.launch 
~~~

Then, check if this calibration is correct, e.g TF square_calib_x is matching with the corner of the table, with : 

~~~
roslaunch optitrack_to_robot run.launch

roslaunch panda_qp_simple_control rviz_only.launch sim:=false debug:=false
~~~

#### How to do

- Launch the .urdf simulation with :

~~~
roslaunch panda_qp_simple_control control_tracking.launch sim:=false load_gripper:=false load_calibration_tip:=true
~~~

- Once everything is launched, press emergency stop button. You must still have an update of the Panda /tf. 
- Check server ip address for Motive
- Start calibration by doing : 

~~~
roslaunch robot_to_frame calibrate_opti.launch

rosservice call /R2TF_calib_trigger
~~~

- Do the calibration process
- Get the transform from table to square_calib_x.
~~~
rosservice call /R2TF_save_tf "table#square_calib_x"
~~~
- Save this transform in $(find optitrack_to_robot)/config/table_to_calibration_square_rc.yaml.

- Check if those values are good by doing : 

~~~
## NB : as for now, the code is made for table_to_calibration_square_rc file. 
## This can be modified in launchfile options. 
roslaunch optitrack_to_robot calibrate.launch 
~~~

~~~
roslaunch optitrack_to_robot run.launch

roslaunch panda_qp_simple_control rviz_only.launch sim:=false debug:=false
~~~

(Do those last steps to check the handmade modifications of your .yaml file, if you want to do some. Recall the difference of orientation between table->square_calib_x and world_optitrack->square_calib_x when doing those changes ).

### Get the transform between the camera and the Optitrack body on the camera

#### How to check

Put pointer tip onto the marker representing the origin of the rigid body (check section "Create a rigid body in Motive which origin is matching the position of a specific marker" to do so). Check if pointer_tip and logitech TF are close or not. This will ensure that the robot ==> optitrack transform is correct.
Then, put the pointer tip on the optical center of the camera. Check if the TF are close or not; it will give you an approximation of the quality of the calibration (BUT : it will not ensure its quality; the theoretical optical frame of the camera do not corresponds to the lens of the camera).

#### How to do

Go to camera_intrinsics_calib readme.md, follow the instructions of "Get camera transform from optical_frame to optitrack_rigid_body_on_optical_frame" section.