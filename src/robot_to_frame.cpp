#include "robot_to_frame/robot_to_frame.h"

// idea: transform between panda->TF (when world->panda and world2->TF)
// note position of pointCloud in TF : PC_TF_L
// point each of those markers in panda body : PC_P
// get association of markers : 
//    * express PC_TF_L and PC_P into local body (SVD) : PC_TF_ICP et PC_P_ICP
//         - Eigen::Matrix4f pcaTransform;
//         - markersManagement::getTransformationMatrixOfPointCloudByPCA(pcaAppliedCloud, pcaTransform );
//         - pcl::transformPointCloud(*pcaAppliedCloud, *notAlignedCloud, pcaTransform.inverse().eval() );
//    * ICP
//          - matchClouds
//          - source : PC_TF_ICP
//          - target : PC_P_ICP
//          - guess : Identity
//    * get associations
//          - icp.correspondences_
//          - index_query : source : PC_TF_ICP
//          - index_match : target : PC_P_ICP
//    * put PC_P in same order as PC_TF
//          - PC_P_assos
// Use the local coordinates of markers in PC_TF, and its values in PC_P, 
// to get the panda->TF transform.
  //  * PC_TF_L -> PC_TF : PC_TF_L * TF
  //  * Panda->PC_P : getTransformationMatrixOfPointCloudByPCA
  //  * PC_P->PC_TF: findTransformWithAssociation (ou transformation ICP)
  //  * PC_TF -> TF : 
// NOT THIS : will give Panda->world2 instead
//  * getPose(world2->TF) : P 
//  * applyTransform(PC_TF_L,P) ==> PC_TF
//  * findTransformWithAssociation    
// Deduce world->world2 = world->Panda->TF->world2 
// Save it to .yaml (call service to save in Python (tU), simpler)

// Test demo :

//  * générer 3 wpts ds moveit : TF1, TF2, TF3 (à partir de des_frame_0)
//  * générer tf_static : world_opti => tf (tq tf = TF1)
//  * demander déplacement entre chaque wpt
//  * check valeur finale : à fin, world_opti doit être placé à des_frame_0.


robotTFCalculator::robotTFCalculator(ros::NodeHandle & nh):
  tU(nh,true),
  PC_TF_L(new pcl::PointCloud<pcl::PointXYZ>),
  PC_TF_L_PCA(new pcl::PointCloud<pcl::PointXYZ>),
  PC_P_L(new pcl::PointCloud<pcl::PointXYZ>),
  PC_P(new pcl::PointCloud<pcl::PointXYZ>)
{

    nh_ = nh;

    // initialize static variables
    calibrationDone = false;
    activateCalib = false;
    gotSamples = false;
    ros::Rate rate(1.0);
    useSynch = typeSynch::USER;

    

    // Services


    // initialize variables from .yaml
    std::string nodeName = ros::this_node::getName();
    nodeName += "/";    

    std::vector< std::vector<double> > localMarkersInTF;
    std::vector<std::string> param_name;
    param_name.push_back("markers");
    rosWrap::getRosParam(param_name,localMarkersInTF, 0, true);
  // std::cout << localMarkersInTF.size() << std::endl;    
  // std::cout << "ok " << std::endl;
    bool useICP = false;
    rosWrap::getRosParam("useICPforCorres",useICP, 1);

    if (useICP)
    {
      useSynch = typeSynch::ICP;
    }

    putTransformInTF = false;
    rosWrap::getRosParam("putTransformInTF",putTransformInTF, 1);



    for (int i = 0; i < localMarkersInTF.size(); i++)
    {
      std::vector<double> v = localMarkersInTF[i];
     //std::cout << v[0] << ";" << v[1] << ";" << v[2] << std::endl;
      pcl::PointXYZ pt(v[0], v[1], v[2]);
      // std::cout << "ok" << std::endl;
      PC_TF_L->points.push_back(pt);

    }

    rosWrap::getRosParam("robot_parent_id",robot_parent_id, 0, true);
    rosWrap::getRosParam("robot_id",robot_id,0 , true);
    rosWrap::getRosParam("TF_id",TF_id,0, true);
    rosWrap::getRosParam("TF_parent_id",TF_parent_id,0 ,true);        



    // initialize markerPubInfo

    // PC_TF (PC in parent TF). (got from .yaml + getPose)
    // pubPCTF.markerPub = nh_.advertise<sensor_msgs::PointCloud2>("PC_TF",1);
    // pubPCTF.rgb[0] = 255; 
    // pubPCTF.rgb[1] = 140; 
    // pubPCTF.rgb[2] = 0; 


    // PC_TF_L (PC by TF in local). (got from .yaml)
    pubPCTFL.markerPub = nh_.advertise<sensor_msgs::PointCloud2>("PC_TF_L",1);
    pubPCTFL.rgb[0] = 50; 
    pubPCTFL.rgb[1] = 0; 
    pubPCTFL.rgb[2] = 255;     

    // PC_TF_L_PCA (PC by TF in local with PCA (to check link)). (got from .yaml)
    pubPCTFLPCA.markerPub = nh_.advertise<sensor_msgs::PointCloud2>("PC_TF_L_PCA",1);
    pubPCTFLPCA.rgb[0] = 140; 
    pubPCTFLPCA.rgb[1] = 0; 
    pubPCTFLPCA.rgb[2] = 140; 
  
    // PC_P_L (PC by Panda in local). (got from user)
    pubPCPL.markerPub = nh_.advertise<sensor_msgs::PointCloud2>("PC_P_L",1);
    pubPCPL.rgb[0] = 0; 
    pubPCPL.rgb[1] = 125; 
    pubPCPL.rgb[2] = 140;     
  

    // PC_P (PC by Panda, as taken by user). (got from computation, yaml and getPose)
    pubPCP.markerPub = nh_.advertise<sensor_msgs::PointCloud2>("PC_P",1);
    pubPCP.rgb[0] = 0; 
    pubPCP.rgb[1] = 255; 
    pubPCP.rgb[2] = 140; 

    // PC_TF_P (PC in Panda, in the same order than PC_TF). (got from computation, yaml and getPose)
    pubPCTFP.markerPub = nh_.advertise<sensor_msgs::PointCloud2>("PC_TF_P",1);
    pubPCTFP.rgb[0] = 255; 
    pubPCTFP.rgb[1] = 0; 
    pubPCTFP.rgb[2] = 0; 

    // Service 

    // Clients
    // Return calibration result
    calibResp = nh.serviceClient<robot_to_frame::ReturnCalib>("/R2TF_resp");

    // Servers
    // Start calibration
    calibAsk = nh.advertiseService("/R2TF_calib",&robotTFCalculator::launchCalibration,this);

    // communication debug only

    //bool canCom = false;
    // robot_to_frame::ReturnCalib srvb;    
    // Eigen::Matrix4f bl; bl.setIdentity();
    // geometry_msgs::Transform blank = tU.toT(bl);
    //         //std::cout << "ok " << std::endl;
    // geometry_msgs::TransformStamped TFb;
    // TFb.header.frame_id = robot_parent_id;
    // TFb.child_frame_id = TF_parent_id; 
    // TFb.header.stamp = ros::Time::now();
    // TFb.transform = blank;

    // bool send = false;

    // srvb.request.transform = TFb;
    // while (ros::ok() and !canCom)
    // {
    //   if (calibResp.exists())
    //   {    
    //     int i = 0;
    //     int Imax = 200;
    //     while (i < Imax)
    //     {
    //       if (calibResp.call(srvb))
    //       {
    //         send = true;
    //         i = Imax;
    //       }
    //       i++;
    //       ros::spinOnce();
    //     }
    //     if(send)
    //     {
    //       canCom = true;
    //     }
    //     else
    //     {
    //       ROS_WARN("Communication failure with return.");
    //     }      
    //   }
    // }



    while (ros::ok())
    {

      if (activateCalib)
      {
        if (!gotSamples)
        {
          takeSamplesFromRobot();
        }
        else
        {
          std::string s = "Please put " + robot_id +" out of TF view " +" and press enter." ;
          std::cout << s << std::endl;
          std::cin >> s;
          // put TF PC in global world
          pcl::PointCloud<pcl::PointXYZ>::Ptr PC_TF(new pcl::PointCloud<pcl::PointXYZ>);
          std::vector<double> L;
          bool success = tU.getPose(TF_parent_id,TF_id,L);
          if (success)
          {
            Eigen::Matrix4f transf = tU.toMf(L);

            pcl::transformPointCloud(*PC_TF_L, *PC_TF, transf );
            // get transform and panda PC in TF PC order, thanks to ICP
            Eigen::Matrix4f tf2r;
            Eigen::Matrix4f r2tf ;
            std::map<int,int> corresST;
            //int result = matchClouds(icpP, PC_TF_L, PC_P, tf2r,corresST,PC_TF_L_PCA, PC_P_L);
            
            // source to target
            int result = matchClouds(icpP, PC_TF, PC_P, tf2r,corresST,PC_TF_L_PCA, PC_P_L);            


            //int result = matchClouds(icpP, PC_P, PC_TF, r2tf,corresST,PC_P_L, PC_TF_L_PCA);            
            if (result < 2)
            {
              // compute parent rob to parent tf

              std::vector<double> L1;            
              std::vector<double> L2;                   
              bool s1 = tU.getPose(TF_id,TF_parent_id,L1,100);
              bool s2 = tU.getPose(robot_parent_id,robot_id,L2,100);
              if (s1 && s2)
              {
                r2tf = tf2r.inverse().eval();

                Eigen::Matrix4f pr2r = tU.toMf(L2);
                Eigen::Matrix4f tf2ptf = tU.toMf(L1);
                //ROS_INFO_STREAM("PR2R : " << pr2r);
                //ROS_INFO_STREAM("tf2ptf : " << tf2ptf);     

                //PR2PTF =  tf2r;
                PR2PTF =  r2tf;

                //ROS_INFO_STREAM("PR2PTF : " << PR2PTF);

                // send transform
                robot_to_frame::ReturnCalib srv;
                geometry_msgs::Transform t = tU.toT(PR2PTF);
                //std::cout << "ok " << std::endl;
                geometry_msgs::TransformStamped TF;
                TF.header.frame_id = robot_parent_id;
                TF.child_frame_id = TF_parent_id; 
                TF.transform = t;
                srv.request.transform = TF;
                bool send = false;                
                if (calibResp.exists())
                {    
                  int i = 0;
                  int Imax = 200;
                  while (i < Imax and ros::ok())
                  {
                    if (calibResp.call(srv))
                    {
                      send = true;
                      i = Imax;
                    }
                    i++;
                    ros::spinOnce();
                  }
                  if(send)
                  {
                    calibrationDone = true;
                    activateCalib=  false;
                  }
                  else
                  {
                    ROS_WARN("Communication failure with return.");
                  }
                } 
              }  
            }
            else
            {
              ROS_INFO("Error associating clouds by ICP");
              gotSamples = false;
            }

          }
          else
          {
            ROS_INFO("Error getting %s.",TF_id.c_str());
          }


        }

      }

      if (calibrationDone)
      {
 
        publishPC(ros::Time::now(), PC_P, pubPCP, robot_parent_id );
        publishPC(ros::Time::now(), PC_TF_L_PCA, pubPCTFLPCA, robot_parent_id );
        publishPC(ros::Time::now(), PC_P_L, pubPCPL, robot_parent_id );
        
        Eigen::Matrix4f transf;
        //bool suc= true;

        bool suc = tU.getPose(TF_parent_id,TF_id,transf);
        if (suc)
        {
          pcl::PointCloud<pcl::PointXYZ>::Ptr PC_TF_P(new pcl::PointCloud<pcl::PointXYZ>);          

          Eigen::Matrix4f t2;
          std::string parent_display;

          if (!putTransformInTF)
          {
            t2 = PR2PTF*transf;
            parent_display = robot_parent_id;
          }
          else
          {
            t2 = transf;
            parent_display = TF_parent_id;
          }

          pcl::transformPointCloud(*PC_TF_L, *PC_TF_P, t2 );

          if (!showedTransf)
          {
            geometry_msgs::Transform t = tU.toT(t2);
            std::cout << "transf : " << transf << std::endl;
            std::cout << "transform of markers (child : "<< TF_id <<" ; parent : " << parent_display << ") : " << t << std::endl;
            showedTransf = true;
          }

          publishPC(ros::Time::now(), PC_TF_P, pubPCTFP, parent_display );          
        }

      }
      publishPC(ros::Time::now(), PC_TF_L, pubPCTFL, robot_parent_id);
      rate.sleep();
      ros::spinOnce();
    }


}


bool robotTFCalculator::launchCalibration(robot_to_frame::AskCalib::Request &req,
               robot_to_frame::AskCalib::Response &res)
{
  if (!activateCalib)
  {

    if (req.TF_id != "")
    {
      TF_id = req.TF_id;
    }
    if (req.TF_parent_id != "")
    {
      TF_parent_id = req.TF_id;
    }
    if (req.robot_id != "")
    {
      robot_id = req.robot_id;
    }
    if (req.robot_parent_id != "")
    {
      robot_parent_id = req.robot_parent_id;
    }
    if (req.local_markers.layout.dim.size() != 0)
    {
      (*PC_TF_L).clear();
      int numMark = req.local_markers.layout.dim[0].size;
      for (int i = 0; i < req.local_markers.data.size(); i+=3)
      {
        pcl::PointXYZ pt(req.local_markers.data[i], req.local_markers.data[i+1], req.local_markers.data[i+2]);
        PC_TF_L->points.push_back(pt);
      }
    }
    rebootCalib();
    activateCalib = true;
  }

  res.success = true;
  ROS_INFO("Got calibration call!");

  return(true);


}


void robotTFCalculator::rebootCalib()
{
  calibrationDone = false;
  showedTransf = false;
  gotSamples = false;
  PR2PTF.setZero();
  (*PC_P_L).clear();
  (*PC_P).clear();
}

void robotTFCalculator::takeSamplesFromRobot()
{
  std::vector<int> spec;
  spec.push_back(255);
  spec.push_back(0);
  spec.push_back(0);  
  int i = 0;
  while ( nh_.ok() && i < PC_TF_L->points.size() )
  {
    std::map<int, std::vector<int> > specialRGB;
    specialRGB[i] = spec;
    publishPC(ros::Time::now(), PC_TF_L, pubPCTFL, robot_parent_id, specialRGB);
    ros::spinOnce();
    std::string s = "Please put " + robot_id +" on marker "+ std::to_string(i) + " and press enter." ;
    std::cout << s << std::endl;
    std::cin >> s;
    std::vector<double> L;
    if (tU.getPose(robot_parent_id,robot_id,L))
    {
      pcl::PointXYZ pt(L[0],L[1], L[2]);
      PC_P->points.push_back(pt);
      i+=1;
    }
    else
    {
      ROS_INFO("Error : cannot find transform %s to %s. Please retry point.", robot_parent_id.c_str(), robot_id.c_str());
    }

  }
  gotSamples = true;
}

void robotTFCalculator::publishPC(ros::Time time, pcl::PointCloud<pcl::PointXYZ>::Ptr PC, markerPubInfo markPub, std::string parent_frame)
{
  std::map<int,std::vector<int> > specialRGB; 
  publishPC(time,PC,markPub,parent_frame,specialRGB);
}

/**
 * @brief Publish the association between the Source and target markers, and the position
 * of the associated target markers.
 * 
 * @param time 
 * @param corresSourceTarget 
 * @param target_markers 
 */

void robotTFCalculator::publishPC(ros::Time time, pcl::PointCloud<pcl::PointXYZ>::Ptr PC, markerPubInfo markPub, std::string parent_frame, std::map<int,std::vector<int> > specialRGB )
{

  std::vector<std::string> names;
  std::vector<int> indexes;
  pcl::PointCloud<pcl::PointXYZRGBL>::Ptr PCPUB(new pcl::PointCloud<pcl::PointXYZRGBL>);

  // create the pointcloud from the map and the caracteristics of the markerPubInfo object
  for (int i = 0; i < PC->points.size(); i++)
  {
      names.push_back("M"+std::to_string(i));
      indexes.push_back(i);
      int rgb[3];
      if (specialRGB.find(i) != specialRGB.end() )
      {
        rgb[0] = specialRGB[i][0];
        rgb[1] = specialRGB[i][1];
        rgb[2] = specialRGB[i][2];                
      }
      else
      {
        rgb[0] =  markPub.rgb[0];
        rgb[1] =  markPub.rgb[1];
        rgb[2] =  markPub.rgb[2];                
      }

      pcl::PointXYZRGBL pt(rgb[0], rgb[1], rgb[2],i);
      pt.x = PC->points[i].x;
      pt.y = PC->points[i].y;
      pt.z = PC->points[i].z;
      (*PCPUB).push_back(pt);
  }

  // send the pointcloud with ROS
  sensor_msgs::PointCloud2 cloud;
  pcl::toROSMsg(*PCPUB,cloud);
  cloud.header.frame_id = parent_frame;
  cloud.header.stamp = time;
  markPub.markerPub.publish(cloud);

}

void robotTFCalculator::printAssociation(std::map< int, int > corresSourceTarget)
{
    int i =0;
    for (std::map<int, int>::iterator it = corresSourceTarget.begin(); it!=corresSourceTarget.end(); it++ )
    {
      std::printf("Source %d ; target associated : %d \n ", it->first, it-> second);
      i++;
    }
}

/**
 * @brief 
 * 
 * @param manParams 
 * @param PC_source
 * @param PC_target
 * @param result_transf transformation from Source to Target (not source_local -> target_local)
 * @param corresSourceTarget 
 * @param PC_target_assos
 * @return int 
 * 
 * NB : maybe add filtering according to type of transform (rotation, translation at first)
 * 
 * NB : q_calib must be set to osMod before executing method.
 * 
 * source : PC_TF_ICP
 * target : PC_P_ICP
 * 
 * 
 */
int robotTFCalculator::matchClouds(
            ICPParams & manParams,
            pcl::PointCloud<pcl::PointXYZ>::Ptr PC_source,
            pcl::PointCloud<pcl::PointXYZ>::Ptr PC_target,
            Eigen::Matrix4f & result_transf,
            std::map< int, int > & corresSourceTarget,
            pcl::PointCloud<pcl::PointXYZ>::Ptr & PC_source_local,
            pcl::PointCloud<pcl::PointXYZ>::Ptr & PC_target_local
             )
{

  int result = 0;
  double min_mark_dist = 99999;

  // compute min distance between markers (PC_source)
  std::map<int,Eigen::Vector3d> mapSource;

  PCL2MapIntEigen(mapSource,PC_source);

  for (std::map<int,Eigen::Vector3d>::iterator it = mapSource.begin(); it!=mapSource.end(); it++ )
  {
    for (std::map<int,Eigen::Vector3d>::iterator it2 = it; it2!=mapSource.end(); it2++ )
    {
      if ( (it->second-it2->second).norm() < min_mark_dist)
      {
        min_mark_dist =  (it->second-it2->second).norm();
      }
    }
  }

  // reput PC in local body (by SVD)
  Eigen::Matrix4f localTS;
  Eigen::Matrix4f localTT;

  getTransformationMatrixOfPointCloudByPCA(PC_source,localTS);
  getTransformationMatrixOfPointCloudByPCA(PC_target,localTT);


  pcl::PointCloud<pcl::PointXYZ>::Ptr sourcePC_local(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::transformPointCloud(*PC_source, *sourcePC_local, localTS.inverse().eval() );

  pcl::PointCloud<pcl::PointXYZ>::Ptr targetPC_local(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::transformPointCloud(*PC_target, *targetPC_local, localTT.inverse().eval() );

  if (useSynch == typeSynch::ICP)
  {
    
    double score = 10;
    int iter = 0;
    int max_iter = 10;

    pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud_ptr(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;

    int max_iters = manParams.icp_max_iter;

    icp.setMaximumIterations(manParams.icp_max_iter);
    icp.setTransformationEpsilon(manParams.transformation_epsilon); // 1e-40
    //icp.setMaxCorrespondenceDistance(min_mark_dist);

    // source : original point cloud
    // target : pointcloud that we want to obtain
    // transform : from source to target

    icp.setInputSource(sourcePC_local);
    icp.setInputTarget(targetPC_local);    

    Eigen::Matrix4f init_guess2;
    init_guess2.setIdentity();

    bool derivation = false;
    if (manParams.icp_verbose)
    {
      ROS_INFO("Start ICP.");
    }

    double max_score  = 1e-4;

    icp.setEuclideanFitnessEpsilon(max_score);

    double best_score = INFINITY;
    Eigen::Matrix4f best_transf;

    while ( ( score > max_score) && (iter < max_iter)     )
    {

      icp.align(*transformed_cloud_ptr, init_guess2); 
      if (manParams.icp_verbose)
      {
        std::cout << "ICP has converged:" << icp.hasConverged()
              << " score: " << icp.getFitnessScore() << std::endl;
      }

      Eigen::Matrix4f final_transf =  icp.getFinalTransformation();
      Eigen::Matrix3f rot = final_transf.block<3,3>(0,0);

      Eigen::Vector3f ea = (rot).eulerAngles(2,0,1);

      if (manParams.icp_verbose)
      {
        std::cout << "Translation : " << final_transf.block<3,1>(0,3) << std::endl;
        std::cout << "Rotation (euler) : " << ea << std::endl;

      }

      if (abs(score - icp.getFitnessScore()) < max_score)
      {
        if (manParams.icp_verbose)
        {
            std::cout << "No more changes. Stop icp." << std::endl;
        }
        iter = max_iter;
      }
      else
      {
        if (score - icp.getFitnessScore() < 0)
        {
          derivation = true;
        }
        else if ( icp.getFitnessScore()< best_score)
        {
          best_score = icp.getFitnessScore();
          best_transf = final_transf;
        }
        
      }

      score = icp.getFitnessScore();

      if ( (score > max_score) && (iter < max_iter)   )
      {
        if (derivation)
        {
          if (manParams.icp_verbose)
          {
              std::cout << "Derivation recorded. Try to fix." << std::endl;
          }
          // TODO add fix in deviation case
        }
      }

      iter++;

    }


    if (icp.hasConverged())
    {

      std::map<int,double> distsourcetarget;
      checkICPResult( 
                    manParams,
                    icp,
                    sourcePC_local,
                    targetPC_local,
                    best_transf,
                    corresSourceTarget,
                    distsourcetarget,
                    true);  
    }

    else
    {
      result = 2;
    }
  }

  else if (useSynch == typeSynch::USER)
  {
    for (int i = 0; i < PC_target->points.size(); i++)
    {
      corresSourceTarget[i] = i;
    }
  }

  //std::cout << "result : " << result << std::endl;
  if (result < 2)
  {
    // return associated points
    pcl::PointCloud<pcl::PointXYZ>::Ptr PC_t_as(new pcl::PointCloud<pcl::PointXYZ>);
    (*PC_target_local).clear();
    for (std::map< int, int >::iterator it = corresSourceTarget.begin(); it!= corresSourceTarget.end(); it++)
    {
      PC_t_as->points.push_back(PC_target->points[it->second]);
    }   


    PC_source_local = sourcePC_local;

    result_transf.setIdentity();
    // return transformation between 2 associated pointClouds 
    pcl::registration::TransformationEstimationSVD<pcl::PointXYZ, pcl::PointXYZ> svd; 
    svd.estimateRigidTransformation(*PC_source,*PC_t_as,result_transf);  
    // poss 2 : 
    // transformation of PC_tar according to ref_tar
    getTransformationMatrixOfPointCloudByPCA(PC_t_as,localTT);
    pcl::transformPointCloud(*PC_t_as,*PC_target_local,localTT.inverse().eval() );
    Eigen::Matrix4f local_transform; local_transform.setIdentity();
    svd.estimateRigidTransformation(*PC_source_local,*PC_target_local, local_transform);


    // transf from source to target
    // pt_Tar = ()
    Eigen::Matrix4f result_transf2 = localTS*local_transform*(localTT.inverse().eval());


    // std::cout << "result transf : " << std::endl;
    // std::cout << result_transf << std::endl;
    Eigen::Matrix3f rot = result_transf.block<3,3>(0,0);

    Eigen::Vector3f ea = (rot).eulerAngles(2,0,1);

    Eigen::Matrix3f rot2 = result_transf2.block<3,3>(0,0);

    Eigen::Vector3f ea2 = (rot2).eulerAngles(2,0,1);    

    if (manParams.icp_verbose)
    {
      std::cout << "local transfrom between PC : " << local_transform << std::endl;
      std::cout << "Translation between pc : " << result_transf.block<3,1>(0,3) << std::endl;
      std::cout << "Rotation between pc (euler) : " << ea << std::endl;
      std::cout << "Translation between pc 2: " << result_transf2.block<3,1>(0,3) << std::endl;
      std::cout << "Rotation between pc 2 (euler) : " << ea2 << std::endl;

    }
    result_transf = result_transf2;

    //result_transf = result_transf.inverse().eval();

  }

  return(result);
};

void robotTFCalculator::findTransformWithAssociation(std::map<int,Eigen::Vector3d> targetPoints, 
std::map<std::string,Eigen::Vector3d> SourcePoints, 
std::map<std::string,int> corresSourceTarget,
Eigen::Matrix4f & transf)
{

    pcl::PointCloud<pcl::PointXYZ>::Ptr sourceCloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr targetCloud(new pcl::PointCloud<pcl::PointXYZ>);

    for (std::map<std::string,Eigen::Vector3d>::iterator it = SourcePoints.begin(); it!= SourcePoints.end(); it++)
    {
      Eigen::Vector3d targetVec = targetPoints[corresSourceTarget[it->first]];
      Eigen::Vector3d sourceVec = it->second;

      pcl::PointXYZ targetPt(targetVec[0],targetVec[1], targetVec[2]);
      pcl::PointXYZ sourcePt(sourceVec[0],sourceVec[1], sourceVec[2]);
      
      sourceCloud->points.push_back(sourcePt);
      targetCloud->points.push_back(targetPt);

    }

    pcl::registration::TransformationEstimationSVD<pcl::PointXYZ, pcl::PointXYZ> svd; 
    svd.estimateRigidTransformation(*sourceCloud,*targetCloud,transf);

}


void robotTFCalculator::mapIntEigen2PCL(std::map<int,Eigen::Vector3d> map, pcl::PointCloud<pcl::PointXYZ>::Ptr & PC)
{
  for (std::map<int,Eigen::Vector3d>::iterator it = map.begin(); it!= map.end(); it++)
  {
    Eigen::Vector3d sourceVec = it->second;
    pcl::PointXYZ sourcePt(sourceVec[0],sourceVec[1], sourceVec[2]);
    PC->points.push_back(sourcePt);
  }
}

void robotTFCalculator::mapStringEigen2PCL(std::map<std::string,Eigen::Vector3d> map, pcl::PointCloud<pcl::PointXYZ>::Ptr & PC, std::vector<std::string> & nameOrder)
{
  int i = 0;
  for (std::map<std::string,Eigen::Vector3d>::iterator it = map.begin(); it!= map.end(); it++)
  {
    nameOrder.push_back(it->first);
    Eigen::Vector3d sourceVec = it->second;
    pcl::PointXYZ sourcePt(sourceVec[0],sourceVec[1], sourceVec[2]);
    PC->points.push_back(sourcePt);
  }
}

void robotTFCalculator::PCL2MapIntEigen(std::map<int,Eigen::Vector3d> & map, pcl::PointCloud<pcl::PointXYZ>::Ptr PC)
{
  map.clear();
  for (int i = 0; i < PC->points.size(); i++)
  {
    Eigen::Vector3d sourceVec(PC->points[i].x, PC->points[i].y, PC->points[i].z);
    map[i] = sourceVec;
  }
}



void robotTFCalculator::getTransformationMatrixOfPointCloudByPCA(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud,
Eigen::Matrix4f & transformation)
{
  Eigen::Vector4f translation;
  pcl::compute3DCentroid(*cloud,translation);
  getTransformationMatrixOfPointCloudByPCA(cloud, transformation, translation); 
}

/**
 * @brief Get the Transformation Matrix of a pointCloud (= orientation and translation according to ground) of a
 * PointCloud by PCA method.
 * 
 * @param Cloud 
 * @param transformation 
 */
void robotTFCalculator::getTransformationMatrixOfPointCloudByPCA(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud,
Eigen::Matrix4f & transformation,
Eigen::Vector4f translation)
{
  // for (int i = 0; i < cloud->points.size(); i++)
  // {
  //   std::printf("Origin position : %3.4f, %3.4f, %3.4f. \n", cloud->points[i].x,cloud->points[i].y,cloud->points[i].z);
  // }

  transformation.setIdentity();
  transformation.block<4,1>(0,3) = translation;
  pcl::PCA<pcl::PointXYZ> pcaX;
  pcaX.setInputCloud(cloud);
  Eigen::Matrix3f XEVs_Dir = pcaX.getEigenVectors();

  // std::cout << "---" << std::endl;
  // std::cout << "Eigen Vectors : " << std::endl;
  // std::cout << XEVs_Dir << std::endl;
  // std::cout << XEVs_Dir.block<3,1>(0,0).cross(XEVs_Dir.block<3,1>(0,1)) << std::endl;
  // std::cout << std::endl;
  // std::cout << XEVs_Dir.block<3,1>(0,1).cross(XEVs_Dir.block<3,1>(0,2)) << std::endl;
  // std::cout << std::endl;
  // std::cout << XEVs_Dir.block<3,1>(0,2).cross(XEVs_Dir.block<3,1>(0,0)) << std::endl;
  // std::cout << "---" << std::endl;

  // get orthonormal matrix using Gram-Schimdt

  Eigen::Vector3f ax1 = XEVs_Dir.block<3,1>(0,0);
  ax1.normalize();
  
  Eigen::Vector3f ev2 = XEVs_Dir.block<3,1>(0,1);
  ev2.normalize();

  Eigen::Vector3f ax2 = ev2 - (ax1.transpose() * ev2) * ax1 / (ax1.transpose()* ax1);
  ax2.normalize();

  Eigen::Vector3f ax3 = ax1.cross(ax2);

  transformation.block<3,1>(0,0) = ax1;
  transformation.block<3,1>(0,1) = ax2;
  transformation.block<3,1>(0,2) = ax3;

  // NB : all vectors in transformation are orthogonal to each of them.
  // std::cout << "---" << std::endl;
  // std::cout << "Referential computed : " << std::endl;
  // std::cout << transformation << std::endl;
  // // std::cout << ax1.dot(ax2) << std::endl;
  // // std::cout << ax2.dot(ax3) << std::endl;
  // // std::cout << ax1.dot(ax3) << std::endl;
  // std::cout << "---" << std::endl;



}

/**
 * 
 * To check : 
 *  - Try with PCA PC
 *  - Put Identity as result transf
 * 
*/
int robotTFCalculator::checkICPResult(ICPParams & manParams,
                      pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp,
                      pcl::PointCloud<pcl::PointXYZ>::Ptr source_PC,
                      pcl::PointCloud<pcl::PointXYZ>::Ptr target_PC,
                      Eigen::Matrix4f result_transf ,
                      std::map< int, int > & corresSourceTarget,
                      std::map<int,double> & distSourceTarget,
                      bool verbose)
{
        corresSourceTarget.clear();

        bool assos_error = false;
        std::vector<int> pc_source_indexes;
        std::vector<int> pc_target_indexes;



        int num_source = source_PC->points.size();
        pcl::CorrespondencesPtr corres = icp.correspondences_;
        double sum = 0;

        int check = 0;
std::map< int, int > cTS;
        // index_match : target
        // index_query : Source
        // source_name : name of all the Source markers by order

        for (int i = 0; i < (*corres).size(); i++)
        {
          pcl::Correspondence cortst= (*corres)[i];
          if (manParams.filtering_verbose)
          {
            std::cout << "Source : " << cortst.index_query << " | target : " << cortst.index_match << " | " << cortst.distance << std::endl; 
          }          
          if ( cTS.find( cortst.index_match ) != cTS.end() )
          {
            // means the source marker has already been associated with another marker, which is an issue.
            // disqualify body part
            assos_error = true;
          }

          else
          {
            corresSourceTarget[cortst.index_query] = cortst.index_match;
            cTS[cortst.index_match] = cortst.index_query;
          }
          
       }

      //  if (num_source != mark_body_parts.size())
      //  {
      //    // the number of theorical Source markers is different from the total of target marker recorded.
      //    // it means that at least one Source marker was not attributed; raise error.
      //    body_part_error = true;

      //  }

      // now we know if there is an error or no. 
      

      // method : rescale source ac scale_factors (ok)
      // source -> target transformation sur source markers (pc)
      // closest, selon meth def plus bas
      if (!assos_error)
      {

        if (manParams.filtering_verbose)
        {   
            printf("---- All good case ---- \n");
            for (std::map<int,int>::iterator it = corresSourceTarget.begin(); it!= corresSourceTarget.end(); it++ )
            {
                pcl::PointXYZ ptS = source_PC->points[it->first];
                pcl::PointXYZ ptT = target_PC->points[it->second];
                
                std::printf("Source ID : %d ; position : [%3.4f, %3.4f, %3.4f] \n ", it->first, ptS.x, ptS.y, ptS.z);
                std::printf("Target ID : %d ; position : [%3.4f, %3.4f, %3.4f] \n ", it->second,  ptT.x, ptT.y, ptT.z);
            }
        }        

      }
      //
      else
      {

        corresSourceTarget.clear();

        // apply pointcloud transformation
        std::cout << "Issue detected ; try to fix it ... " << std::endl;
    
        pcl::PointCloud<pcl::PointXYZ>::Ptr sourceMovedPC(new pcl::PointCloud<pcl::PointXYZ>);
        pcl::transformPointCloud(*source_PC, *sourceMovedPC, result_transf);


        if (manParams.filtering_verbose)
        {
            printf("---- Source positions (after transformation) ---- \n");
            for (int i = 0; i < sourceMovedPC->points.size(); i++ )
            {
              pcl::PointXYZ ptS = sourceMovedPC->points[i];
              std::printf("Source ID : %d ; position : [%3.4f, %3.4f, %3.4f] \n ", i, ptS.x, ptS.y, ptS.z);

            }

            printf("---- Target positions ---- \n");
            for (int i = 0; i < target_PC->points.size(); i++ )
            {
              pcl::PointXYZ ptT = target_PC->points[i];
              std::printf("Target ID : %d ; position : [%3.4f, %3.4f, %3.4f] \n ", i,  ptT.x, ptT.y, ptT.z);
            }

        }

        check = 1;

        int sourceSize = sourceMovedPC->points.size();
        int targetSize = target_PC->points.size();      

        std::map<int,Eigen::Vector3d> mapSource;
        std::map<int,Eigen::Vector3d> mapTarget;
        PCL2MapIntEigen(mapSource,sourceMovedPC);
        PCL2MapIntEigen(mapTarget, target_PC);        
        // for each body part concerned, search the closest target markers

        std::map< double, std::vector<int> > D_dist_couple;
        std::vector<double> all_dist;

        // check distance of each Source marker to each target marker
        int j = 0;
        for (std::map<int,Eigen::Vector3d>::iterator it = mapSource.begin(); it!= mapSource.end(); it++)
        {
          Eigen::Vector3d source_mark = it->second;
          int k = 0;
          for (std::map<int,Eigen::Vector3d>::iterator it2 = mapTarget.begin(); it2!= mapTarget.end(); it2++)
          {
            Eigen::Vector3d target_pos = it2->second;
            double dist = (source_mark - target_pos).norm();

            std::vector<int> v = {j,k};
            std::map< double, std::vector<int> >::iterator it3;
            it3 = D_dist_couple.find(dist);
            if (it3 != D_dist_couple.end() )
            {
              v = it3->second;
              v.push_back(j);
              v.push_back(k);
              D_dist_couple.erase(it3);

            }
            D_dist_couple[dist] = v;
            all_dist.push_back(dist);
            k+=1;

          }
          j+= 1;

        }




        int target_id = 0;
        int source_id = 0;

        bool done = false;

        //while (target_id < targetPoints.size() )
        // continuer check
        while (mapSource.size()> 0 && mapTarget.size() > 0)
        {

          // one target marker has been selected by target_id
          std::sort(all_dist.begin(), all_dist.end());

          double selected_dist = all_dist[0];
          std::cout << "selected_dist : " << selected_dist << std::endl;

          std::vector<int> couples = D_dist_couple[selected_dist];
          int l = 0;
          while (l < couples.size())
          {
            std::map<int,Eigen::Vector3d>::iterator itS = mapSource.find(couples[l]);
            std::map<int,Eigen::Vector3d>::iterator itT = mapTarget.find(couples[l+1]);     
            if (itS != mapSource.end() && itT != mapTarget.end())     
            {
              // associate source and target points
              corresSourceTarget[ couples[l]] = couples[l+1];
              mapSource.erase(itS);
              mapTarget.erase(itT);
            }

            l+=2;
          }
          D_dist_couple.erase( D_dist_couple.find(selected_dist) );
          all_dist.erase(  all_dist.begin());

        }

        if (mapSource.size() != mapTarget.size())
        {
          check = 2;
        }
        
        if (manParams.filtering_verbose) {printAssociation(corresSourceTarget);}

      }

    //display results
    if (verbose)
    {
      std::printf(" ---- FINAL ---- \n");
      printAssociation(corresSourceTarget);
    }


    return(check);


}

int main(int argc, char **argv)
{
  /**
   * The ros::init() function needs to see argc and argv so that it can perform
   * any ROS arguments and name remapping that were provided at the command line.
   * For programmatic remappings you can use a different version of init() which takes
   * remappings directly, but for most command-line programs, passing argc and argv is
   * the easiest way to do it.  The third argument to init() is the name of the node.
   *
   * You must call one of the versions of ros::init() before using any other
   * part of the ROS system.
   */
  ros::init(argc, argv, "r2f_core");
  ros::NodeHandle nh;

  robotTFCalculator rtc(nh);
  return 0;
}