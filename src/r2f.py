#!/usr/bin/env python3

import rospy
import numpy as np
from robot_to_frame.srv import AskCalib,AskCalibRequest,ReturnCalib
from tf_management_pkg.tf_utils import tfUtils
from std_srvs.srv import Empty

from ros_wrap.com_manager import comManager

from msg_srv_action_gestion.srv import SetString
import geometry_msgs.msg
import tf2_ros

class R2F():

    def __init__(self):
        rospy.init_node('R2F')

        nodeName = rospy.get_name()
        nodeName+="/"

        self.tf_path = rospy.get_param(nodeName+"output_save","")   
        # self.tf_path = rospy.get_param(nodeName+"output_save","vS_frame")   
        # self.tf_path = rospy.get_param(nodeName+"output_save","vS_frame")   
        # self.tf_path = rospy.get_param(nodeName+"output_save","vS_frame")   
        # self.tf_path = rospy.get_param(nodeName+"output_save","vS_frame")   

        self.broadcastTF = rospy.get_param("putTransformInTF",False) 

        self.tU = tfUtils(True)
        self.cM = comManager()

        ## Client        
        self.ask_calib = rospy.ServiceProxy("/R2TF_calib",AskCalib)

        ## Server
        self.calib_resp =  rospy.Service("/R2TF_resp",ReturnCalib, self.get_calib_return)
        self.calib_start =  rospy.Service("/R2TF_calib_trigger",Empty, self.trigger_calib)
        self.resp_save_tf =  rospy.Service("/R2TF_save_tf",SetString, self.save_tf)

        self.possible_state = ["IDLE","AskCalib","W4R","AC"]
        self.state = "IDLE"

        self.st_br_A = tf2_ros.StaticTransformBroadcaster() 


        rate = rospy.Rate(10.0)
        while not rospy.is_shutdown():

            if (self.state == "AskCalib"):
                srv = AskCalibRequest()
                suc,resp,ret = self.cM.call_service("R2TF_calib",self.ask_calib,srv)
                if (resp == 1):
                    if (ret.success):
                        self.state = "W4R"
                        #rospy.loginfo("Wait for calibration to end...")
                    else:
                        self.state = "IDLE"                        
                else:
                    self.state = "IDLE"
            # if (self.state == "IDLE" or self.state == "W4R"):

            rate.sleep()


    def save_tf(self,msg):
        if (self.state == "AC"):
            L = msg.data.split("#")
            parent = L[0]
            child = L[1]
            P,suc = self.tU.getPose(parent,child)
            if (suc):
                name = self.tf_path + "/%s_to_%s.yaml"%(parent,child)
                self.tU.saveTransformToYaml(P, child,parent, name)



        return(True)



    def get_calib_return(self,msg):
        rospy.loginfo("Got calib return!")
        print(msg.transform)

        name = self.tf_path + "/%s_to_%s.yaml"%(msg.transform.header.frame_id, msg.transform.child_frame_id)

        #self.tU.saveTransformToYaml(self.tU.T2L(msg.transform.transform), msg.transform.child_frame_id, msg.transform.header.frame_id, name)
        if self.broadcastTF:
            static_transformStamped_A = msg.transform
            self.st_br_A.sendTransform(static_transformStamped_A)

        self.state = "AC"

        return(True)

    def trigger_calib(self,msg):
        self.state = "AskCalib"
        return()


if __name__ == "__main__":
    r2f = R2F()