
#ifndef robotToAnyTF_h
#define robotToAnyTF_h


#include <vector>

#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Geometry>
#include <eigen3/Eigen/LU>
#include <eigen3/Eigen/Eigenvalues> 
#include <pcl/registration/icp.h>
#include <pcl/registration/transformation_estimation_svd.h>
#include <pcl/common/pca.h>
#include <pcl/common/common.h>
#include <pcl/conversions.h>
#include <pcl_conversions/pcl_conversions.h>

#include "ros/ros.h"

#include "ros_wrap/rosparam_getter.h"

#include "robot_to_frame/AskCalib.h"
#include "robot_to_frame/ReturnCalib.h"

#include "tf_management_pkg/tf_utils.h"

enum typeSynch {ICP,USER};

struct ICPParams
{

    double transformation_epsilon = 1e-40;
    double euclidean_fitness_epsilon = 1e-40;
    int icp_max_iter = 1000;

    bool icp_verbose = true;
    bool filtering_verbose = true;


    //   maximum_translation = 2.0;
    //   maximum_rotation = 5*M_PI/180;

};

struct markerPubInfo
{
    // ROS publisher object of the point cloud.
    ros::Publisher markerPub;
    // Color associated with the point cloud (rgb format)
    int rgb[3];
};


class robotTFCalculator
{

    public:
        robotTFCalculator(ros::NodeHandle & nh);

        bool launchCalibration(robot_to_frame::AskCalib::Request &req,
                    robot_to_frame::AskCalib::Response &res);
        void rebootCalib();
        void takeSamplesFromRobot();
        void publishPC(ros::Time time, pcl::PointCloud<pcl::PointXYZ>::Ptr PC, markerPubInfo markPub, std::string parent_frame);
        void publishPC(ros::Time time, pcl::PointCloud<pcl::PointXYZ>::Ptr PC, markerPubInfo markPub, std::string parent_frame, std::map<int,std::vector<int> > specialRGB );
        void printAssociation(std::map< int, int > corresSourceTarget);
        int matchClouds(
            ICPParams & manParams,
            pcl::PointCloud<pcl::PointXYZ>::Ptr PC_source,
            pcl::PointCloud<pcl::PointXYZ>::Ptr PC_target,
            Eigen::Matrix4f & result_transf,
            std::map< int, int > & corresSourceTarget,
            pcl::PointCloud<pcl::PointXYZ>::Ptr & PC_target_local,
            pcl::PointCloud<pcl::PointXYZ>::Ptr & PC_source_local
             );
        void findTransformWithAssociation(std::map<int,Eigen::Vector3d> targetPoints, 
            std::map<std::string,Eigen::Vector3d> SourcePoints, 
            std::map<std::string,int> corresSourceTarget,
            Eigen::Matrix4f & transf);
        void mapIntEigen2PCL(std::map<int,Eigen::Vector3d> map, pcl::PointCloud<pcl::PointXYZ>::Ptr & PC);
        void mapStringEigen2PCL(std::map<std::string,Eigen::Vector3d> map, pcl::PointCloud<pcl::PointXYZ>::Ptr & PC, std::vector<std::string> & nameOrder);
        void PCL2MapIntEigen(std::map<int,Eigen::Vector3d> & map, pcl::PointCloud<pcl::PointXYZ>::Ptr PC);
        void getTransformationMatrixOfPointCloudByPCA(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud,
            Eigen::Matrix4f & transformation);
        void getTransformationMatrixOfPointCloudByPCA(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud,
            Eigen::Matrix4f & transformation,
            Eigen::Vector4f translation);
        int checkICPResult( ICPParams & manParams,
                            pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp,
                            pcl::PointCloud<pcl::PointXYZ>::Ptr source_PC,
                            pcl::PointCloud<pcl::PointXYZ>::Ptr target_PC,
                            Eigen::Matrix4f result_transf ,
                            std::map< int, int > & corresSourceTarget,
                            std::map<int,double> & distSourceTarget,
                            bool verbose);

        pcl::PointCloud<pcl::PointXYZ>::Ptr PC_TF_L;
        pcl::PointCloud<pcl::PointXYZ>::Ptr PC_TF_L_PCA;        
        // panda points in local + in same order as TF
        pcl::PointCloud<pcl::PointXYZ>::Ptr PC_P_L;    
        // PC in panda
        pcl::PointCloud<pcl::PointXYZ>::Ptr PC_P;                   
        ICPParams icpP;

       // markerPubInfo pubPCTF;
        markerPubInfo pubPCTFL;
        markerPubInfo pubPCP;
        markerPubInfo pubPCTFP;        
        markerPubInfo pubPCPL;
        markerPubInfo pubPCTFLPCA;
        std::map<std::string,std::string> D_parent_child;

        bool calibrationDone;
        bool activateCalib;
        bool gotSamples;
        

        std::string TF_id;
        std::string TF_parent_id;
        std::string robot_id;
        std::string robot_parent_id;      

        tfUtils tU;

        ros::ServiceClient calibResp;
        ros::ServiceServer calibAsk;

        Eigen::Matrix4f PR2PTF;

        ros::NodeHandle nh_;

        typeSynch useSynch;

        bool showedTransf = false;

        bool putTransformInTF;

};

#endif